<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION
	Most commonly contains all the top level pages and search options.

\*----------------------------------------------------------------*/
?>

<div class="navigation-block">
	<a href="<?php echo get_home_url(); ?>">
		<svg>
			<use xlink:href="#logo" />
		</svg>
	</a>
	<div class="hamburger-menu"></div>
	<nav>
		<?php wp_nav_menu(array( 'theme_location' => 'primary_nav' )); ?>
	</nav>
</div>