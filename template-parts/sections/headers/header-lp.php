<?php 
/*----------------------------------------------------------------*\

	HEADER WITH IMAGE BACKGROUND FOR LP

\*----------------------------------------------------------------*/
?>

<header class="page-title has-image">
	<section>

		<div>
			<h1><?php if ( get_field('page_title') ) : the_field('page_title'); else : the_title(); endif; ?></h1>

			<?php if ( get_field('title_description') ) : ?>
				<p><?php the_field('title_description'); ?></p>
			<?php endif; ?>
		</div>

		<div>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/navya-shuttle.png" alt="Navya Shuttle">
		</div>

	</section>

</header>