<?php 
/*----------------------------------------------------------------*\

	HEADER WITH VIDEO BACKGROUND

\*----------------------------------------------------------------*/
?>

<header class="page-title has-video" style="background-image: url('<?php the_field('title_bg_img'); ?>');">
	<section>

		<h1>
			<?php 
				if ( get_field('page_title') ) :
					the_field('page_title');
				else :
					the_title();
				endif;
			?>
		</h1>

		<?php if ( get_field('title_description') ) : ?>
			<p>
				<?php the_field('title_description'); ?>
			</p>
		<?php endif; ?>

		<?php if ( have_rows('title_buttons') ) : ?>
			<div class="buttons">
				<?php while( have_rows('title_buttons') ) : the_row(); ?>
					<?php $button = get_sub_field('button'); ?>
					<a class="button" href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>">
						<?php echo $button['title']; ?>
					</a>
				<?php endwhile; ?>
			</div>
		<?php endif; ?>

	</section>

	<div class="overlay"></div>

	<video muted="" autoplay="" loop="" poster="" class="bgvid"> 
		<source src="<?php the_field('title_bg_vid'); ?>" type="video/mp4"> 
	</video>

</header>