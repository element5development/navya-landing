<?php 
/*----------------------------------------------------------------*\

	FULL WIDTH BANNER

\*----------------------------------------------------------------*/
?>


<section class="banner" style="background-image: url('<?php the_field('banner_bg_img'); ?>');">

	<div>
		<?php if ( get_field('banner_title') ) : ?>
			<h2><?php the_field('banner_title'); ?></h2>
		<?php endif; ?>

		<?php if ( get_field('banner_description') ) : ?>
			<p><?php the_field('banner_description'); ?></p>
		<?php endif; ?>

		<?php if ( have_rows('banner_buttons') ) : ?>
			<div class="buttons">
				<?php while( have_rows('banner_buttons') ) : the_row(); ?>
					<?php $button = get_sub_field('button'); ?>
					<a class="button" href="<?php echo $button['url']; ?>" target="<?php echo $button['target']; ?>">
						<?php echo $button['title']; ?>
					</a>
				<?php endwhile; ?>
			</div>
		<?php endif; ?>
	</div>

</section>