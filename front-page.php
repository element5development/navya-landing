<?php 
/*----------------------------------------------------------------*\

	Template Name: Landing Page
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation-lp'); ?>

<?php get_template_part('template-parts/sections/headers/header-lp'); ?>

<div id="canvas-wrapper"></div>

<main>

	<article>
		<?php if ( '' !== get_post()->post_content ) : ?>
			<section class="wysiwyg-block">
				<?php the_content(); ?>
			</section>
		<?php endif; ?>
	</article>
	
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>