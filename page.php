<?php 
/*----------------------------------------------------------------*\

	DEFAULT PAGE TEMPLATE
	Standard page template for website which should include all the
	commonly used options.

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php
	if ( get_field('title_bg_vid') ) :
		get_template_part('template-parts/sections/headers/header-video');
	else :
		get_template_part('template-parts/sections/headers/header');
	endif;
?>


<?php if ( function_exists('yoast_breadcrumb') ) { 
	yoast_breadcrumb('<nav class="breadcrumbs">','</nav>'); 
} ?>

<main>

	<article>
		<?php if ( '' !== get_post()->post_content ) : ?>
			<section class="wysiwyg-block">
				<?php the_content(); ?>
			</section>
		<?php endif; ?>
		<?php if ( get_field('banner_title') ) : 
			get_template_part('template-parts/sections/banner');
		endif; ?>
		<?php if ( get_field('gallery') ) : 
			get_template_part('template-parts/sections/gallery');
		endif; ?>
	</article>
	
</main>

<?php get_template_part('template-parts/sections/icon-slider'); ?>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>