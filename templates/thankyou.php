<?php 
/*----------------------------------------------------------------*\

	Template Name: Thank You
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<main>
	<article>
		<section class="wysiwyg-block">
			<!-- THIS IS JUST A PLACEHOLDER MUST SWAP IMAGE -->
				<img src="https://cdn.dribbble.com/users/784413/screenshots/3818347/mail.gif" alt="message recieved" />
			<!-- THIS IS JUST A PLACEHOLDER MUST SWAP IMAGE -->
			<h1><?php the_field('page_title'); ?></h1>
			<p><?php the_field('title_description'); ?></p>
			<div class="buttons">
				<a class="button is-primary" href="/">Return to the Homepage</a>
				<a class="button is-ghost" href="/">Contact Support</a>
			</div>
		</section>
	</article>
</main>

<?php get_template_part('template-parts/sections/icon-slider'); ?>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>